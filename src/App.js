import React from 'react'
import Fakebook from './pages/Fakebook'
import './components/styles/App.css'

function App() {
  return (
    <div className="App">
      <Fakebook/>
    </div>
  );
}
export default App