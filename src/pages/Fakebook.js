import React from 'react'
import Buscador from '../components/fakebook/Buscador'
import Menus from '../components/fakebook/Menus'
import Feed from '../components/fakebook/Feed';

const Fakebook = () => {    
    return (
        <div>
            <Buscador/>
            <Menus/>
            <Feed/>
        </div>
    )
}

export default Fakebook



// Documentación
// class Fakebook extends React.Component {
//      TODO:
// }

// const Fakebook = () => {
//      TODO:
// }