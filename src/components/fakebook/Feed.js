import React from 'react'
import Publicacion from './Publicacion'
import publicaciones from '../../data/publicaciones.json'

const Feed = () => {
    return (
        <div>
        {publicaciones.map((data)=>{
            return(
                <Publicacion
                    avatar = {data.avatar}
                    usuario = {data.usuario}
                    horario = {data.horario}
                    descFoto = {data.descFoto}
                    fotoSrc = {data.fotoSrc}
                    reacciones = {data.reacciones}
                />
            )
        })
        }
        </div>
    )
}

export default Feed