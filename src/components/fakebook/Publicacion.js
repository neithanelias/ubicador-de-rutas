import React from 'react'
import '../styles/Publicacion.css'
import Reacciones from './Reacciones'

const Publicacion = (props) => {
    return (
        <div className="divPublicacion">
                <div className="usuario">
                    <div className="usuarioAvatar">
                        <img src={props.avatar} alt=""/>
                    </div>
                    <div className="usuarioDescrip">
                        <h3>{props.usuario}</h3>
                        <p>{props.horario}</p>
                    </div>
                </div>
                <div className="contenidoPub">
                    <p>{props.descFoto}</p>
                    <img src={props.fotoSrc} alt=""/>
                </div>        
                <div className="divReacciones">
                    <p>{props.reacciones}</p>
                </div>
                <div>
                    <Reacciones/>
                </div>
        </div>
    )
}

export default Publicacion
