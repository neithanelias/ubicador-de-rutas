import React from 'react'
import '../styles/Reacciones.css'

const Reacciones = () => {
    return (
        <div>
        <div className="botonesReacciones">
            <div className="socialMediaBtn"><img src="http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/32/Thumb-up-icon.png" alt=""/><span>Me gusta</span></div>
            <div className="socialMediaBtn"><img src="http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-9/32/comment-edit-icon.png" alt=""/><span>Comentar</span></div>
            <div className="socialMediaBtn"><img src="http://icons.iconarchive.com/icons/google/noto-emoji-travel-places/32/42598-rocket-icon.png" alt=""/><span>Compartir</span></div>
        </div>
        </div>
    )
}

export default Reacciones
